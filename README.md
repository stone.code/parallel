# parallel
> Easy concurrent loops.

Package parallel provides a runner to run tasks with limited concurrency.
Using this package, it should be straightforward to replace any loop with
similar code that provides concurrency.

## Installation

As with most projects for [Go](https://golang.org), installation is as easy as `go get`.

## Documentation

Please refer to the documentation on [godoc.org](https://godoc.org/gitlab.com/stone.code/parallel) for API documentation.

An example of how the package can be used is below.  The full source can be found on the [Go playground](https://play.golang.org/p/w-HQq8V6PkT).

```
urls := []string{
    "http://www.golang.org/",
    "http://www.google.com/",
    "http://www.somestupidname.com/",
}

// Build a runner using the default context, and which will limit
// concurrency to the number of CPUs. 
r := parallel.NewRunner(nil)
for _, url := range urls {
    url := url
    r.Go(func() {
        // Fetch the URL.
        http.Get(url)
    })
}
// Wait for all HTTP fetches to complete.
r.Wait()
```

## Contributing

Development of this project is ongoing.  If you find a bug or have any suggestions, please [open an issue](https://gitlab.com/stone.code/parallel/issues).

If you'd like to contribute, please fork the repository and make changes.  Pull requests are welcome.

## Related projects and documentation

- [golang.org/x/sync/errgroup](https://godoc.org/golang.org/x/sync/errgroup):  Package errgroup provides synchronization, error propagation, and Context cancelation for groups of goroutines working on subtasks of a common task.
- [github.com/juju/utils/parallel](https://godoc.org/github.com/juju/utils/parallel):  The parallel package provides utilities for running tasks concurrently.
- [sync.WaitGroup](https://godoc.org/sync#WaitGroup):  A WaitGroup waits for a collection of goroutines to finish.
- [Worker Pools](https://gobyexample.com/worker-pools):  An example on how to implement a worker pool using goroutines and channels, provided by [Go by Example](https://gobyexample.com/).

## Licensing

This project is licensed under the [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause).  See the LICENSE in the repository.
