package parallel_test

import (
	"bytes"
	"context"
	"fmt"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/stone.code/parallel"
)

func TestRunner(t *testing.T) {
	cnt := uint32(0)

	r := parallel.NewRunner(nil)
	if got := r.Context(); got != context.Background() {
		t.Errorf("runner did not provide default context")
	}
	if got := r.MaxConcurrency(); got < 1 {
		t.Errorf("runner did not provide reasonable max concurrency, got %d", got)
	}
	t.Logf("MaxConcurrency = %d", r.MaxConcurrency())
	r.Go(func() {
		atomic.AddUint32(&cnt, 1)
	})
	r.Go(func() {
		atomic.AddUint32(&cnt, 2)
	})
	r.Wait()

	if got := atomic.LoadUint32(&cnt); got != 3 {
		t.Errorf("Expected 3, got %d", got)
	}
}

func TestNewRunnerWithMax(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	r := parallel.NewRunnerWithMax(ctx, 3)
	if got := r.Context(); got != ctx {
		t.Errorf("runner did not provide the expected context")
	}
	if got := r.MaxConcurrency(); got != 3 {
		t.Errorf("runner did not provide the expected max concurrency, got %d", got)
	}
}

func TestRunnerWithZeroMax(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Did not get runtime panic")
		}
	}()

	_ = parallel.NewRunnerWithMax(context.Background(), -1)
	t.Errorf("unreachable")
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func TestRunner_Concurrency(t *testing.T) {
	// Will vary concurrency up to the number of reported CPUs, but want to
	// run up to at least 4.
	numcpu := max(4, runtime.NumCPU())
	for i := 1; i <= numcpu; i++ {
		i := i
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			concurrency := int32(0)
			max := int32(0)
			mumax := sync.Mutex{}

			update := func() {
				mumax.Lock()
				defer mumax.Unlock()

				if c := atomic.LoadInt32(&concurrency); c > max {
					max = c
				}
			}

			r := parallel.NewRunnerWithMax(nil, i)
			for j := 0; j < 2*i; j++ {
				r.Go(func() {
					atomic.AddInt32(&concurrency, 1)
					defer atomic.AddInt32(&concurrency, -1)

					for i := 0; i < 4; i++ {
						update()
						time.Sleep(20 * time.Millisecond)
					}
					update()
				})
			}
			r.Wait()

			if max != int32(i) {
				t.Errorf("Failed to hit the right concurrency level, want %d, got %d", i, max)
			}
		})
	}
}

func TestRunner_Loops(t *testing.T) {
	for i := uint32(0); i < 8; i++ {
		i := i
		t.Run(strconv.Itoa(int(i)), func(t *testing.T) {
			cnt := uint32(0)

			r := parallel.NewRunner(nil)
			if got := r.Context(); got != context.Background() {
				t.Errorf("runner did not provide default context")
			}
			if i := r.MaxConcurrency(); i < 1 {
				t.Errorf("runner did not provide reasonable max concurrency, got %d", i)
			}
			for j := uint32(0); j < i; j++ {
				j := j
				r.Go(func() {
					atomic.AddUint32(&cnt, 1<<j)
				})
			}
			r.Wait()

			expected := uint32(1)<<i - 1
			if got := atomic.LoadUint32(&cnt); got != expected {
				t.Errorf("Expected %d, got %d", expected, got)
			}
		})
	}
}

func TestRunner_Cancel(t *testing.T) {
	cnt := uint32(0)

	ctx, cancel := context.WithCancel(context.Background())
	r := parallel.NewRunner(ctx)
	if got := r.Context(); got != ctx {
		t.Errorf("runner did not use correct context")
	}
	if i := r.MaxConcurrency(); i < 1 {
		t.Errorf("runner did not provide reasonable max concurrency, got %d", i)
	}
	r.Go(func() {
		atomic.AddUint32(&cnt, 1)
	})
	cancel()
	r.Go(func() {
		atomic.AddUint32(&cnt, 2)
	})
	r.Wait()

	if got := atomic.LoadUint32(&cnt); got != 1 {
		t.Errorf("Expected 1, got %d", got)
	}
}

func TestRunner_Timeout(t *testing.T) {
	cnt := uint32(0)

	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()
	r := parallel.NewRunnerWithMax(ctx, 1)
	r.Go(func() {
		atomic.AddUint32(&cnt, 1)
		time.Sleep(100 * time.Millisecond)
		atomic.AddUint32(&cnt, 2)
	})
	r.Go(func() {
		atomic.AddUint32(&cnt, 4)
		time.Sleep(100 * time.Millisecond)
		atomic.AddUint32(&cnt, 8)
	})
	r.Wait()

	if err := ctx.Err(); err != context.DeadlineExceeded {
		panic("unexpected exit from context")
	}

	if got := atomic.LoadUint32(&cnt); got != 3 {
		t.Errorf("Expected 3, got %d", got)
	}
}

func TestRunner_Parallel(t *testing.T) {
	buffer := bytes.NewBuffer(nil)
	mu := sync.Mutex{}

	r := parallel.NewRunnerWithMax(context.Background(), 2)
	if got := r.MaxConcurrency(); got != 2 {
		t.Errorf("Expected 2, got %d", got)
	}
	cha := make(chan struct{}, 1)
	chb := make(chan struct{}, 1)
	cha <- struct{}{}
	r.Go(func() {
		for i := 0; i < 3; i++ {
			<-cha
			mu.Lock()
			buffer.WriteString("a")
			mu.Unlock()
			chb <- struct{}{}
		}
	})
	r.Go(func() {
		for i := 0; i < 3; i++ {
			<-chb
			mu.Lock()
			buffer.WriteString("b")
			mu.Unlock()
			cha <- struct{}{}
		}
	})
	r.Wait()

	if got := buffer.String(); got != "ababab" {
		t.Errorf("Expected 'ababab', got %s", got)
	}
}

func ExampleNewRunnerWithMax() {
	buffer := bytes.NewBuffer(nil)

	// Note that we set the maximum concurrency to one.  This is required to
	// get deterministic behaviour for the test.  If set to a higher number,
	// then the order of two output lines may be reversed.
	r := parallel.NewRunnerWithMax(context.Background(), 1)
	r.Go(func() {
		buffer.WriteString("one\n")
	})
	r.Go(func() {
		buffer.WriteString("two\n")
	})
	r.Wait()

	fmt.Println(buffer.String())

	// Output:
	// one
	// two
}

func Example_loop() {
	// Example of a simple loop.
	sum1 := uint32(0)
	for i := 0; i < 10; i++ {
		// The use of atomic is unnecessary here, but is used to keep the body
		// the same as the concurrent loop below.
		atomic.AddUint32(&sum1, uint32(i))
	}

	// Modified loop where iterations can run concurrently.
	sum2 := uint32(0)
	r := parallel.NewRunner(nil)
	for i := 0; i < 10; i++ {
		i := i // Need a copy of the current value for the closure.
		r.Go(func() {
			atomic.AddUint32(&sum2, uint32(i))
		})
	}
	r.Wait()

	fmt.Println(sum1, sum2)

	// Output:
	// 45 45
}

func Example_loopWithBreak() {
	// Example of a simple loop, which uses a break.
	sum1 := uint32(0)
	for i := 0; i < 10; i++ {
		if i == 5 {
			break
		}
		atomic.AddUint32(&sum1, uint32(i))
	}

	// Modified loop where iterations can run concurrently.
	sum2 := uint32(0)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	r := parallel.NewRunner(ctx)
	for i := 0; i < 10; i++ {
		i := i
		r.Go(func() {
			if i >= 5 {
				// Note: that using i == 5 to cancel would lead to non-
				// determinism.  A separate goroutine may add 6..10 before
				// the call to cancel.
				// Note:  this works is practice, but still contains non-
				// determinism.  There is no guarantee that numbers smaller
				// than 5 will be executed first.
				cancel()
				return
			}
			atomic.AddUint32(&sum2, uint32(i))
		})
	}
	r.Wait()

	fmt.Println(sum1, sum2)

	// Output:
	// 10 10
}

func Example_loopWithError() {
	// Example of a simple loop, which uses a break.
	sum1, err1 := func() (uint32, error) {
		sum := uint32(0)
		for i := 0; i < 10; i++ {
			if i == 5 {
				return 0, fmt.Errorf("dummy error")
			}
			atomic.AddUint32(&sum, uint32(i))
		}
		return sum, nil
	}()

	// Modified loop where iterations can run concurrently.
	// Note, unlike above loop, it is possible that loop body will run for i>5.
	sum2, err2 := func() (uint32, error) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		sum := uint32(0)
		r := parallel.NewRunner(ctx)
		errs := make(chan error, r.MaxConcurrency())
		for i := 0; i < 10; i++ {
			i := i
			r.Go(func() {
				if i == 5 {
					errs <- fmt.Errorf("dummy error")
					cancel()
					return
				}
				atomic.AddUint32(&sum, uint32(i))
			})
		}
		r.Wait()

		err, ok := <-errs
		if ok {
			return 0, err
		}

		return sum, nil
	}()

	fmt.Println("A:", sum1, err1)
	fmt.Println("B:", sum2, err2)

	// Output:
	// A: 0 dummy error
	// B: 0 dummy error
}
