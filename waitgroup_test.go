package parallel_test

import (
	"net/http"
	"sync"
	"sync/atomic"
	"testing"

	"gitlab.com/stone.code/parallel"
)

func ExampleWaitGroup() {
	var wg parallel.WaitGroup
	var urls = []string{
		"http://www.golang.org/",
		"http://www.google.com/",
		"http://www.somestupidname.com/",
	}
	for _, url := range urls {
		url := url
		wg.Go(func() {
			// Error ignored in example
			_, _ = http.Get(url)
		})
	}
	// Wait for all HTTP fetches to complete.
	wg.Wait()
}

func TestWaitGroup(t *testing.T) {
	cnt := uint32(0)

	r := parallel.WaitGroup{}
	r.Go(func() {
		atomic.AddUint32(&cnt, 1)
	})
	r.Go(func() {
		atomic.AddUint32(&cnt, 2)
	})
	r.Wait()

	if got := atomic.LoadUint32(&cnt); got != 3 {
		t.Errorf("Expected 3, got %d", got)
	}
}

func BenchmarkSyncWaitGroup(b *testing.B) {
	type PaddedWaitGroup struct {
		sync.WaitGroup
		pad [128]uint8
	}
	b.RunParallel(func(pb *testing.PB) {
		var wg PaddedWaitGroup
		for pb.Next() {
			wg.Add(1)
			go func() {
				wg.Done()
			}()
			wg.Wait()
		}
	})
}

func BenchmarkWaitGroup(b *testing.B) {
	type PaddedWaitGroup struct {
		parallel.WaitGroup
		pad [128]uint8
	}
	b.RunParallel(func(pb *testing.PB) {
		var wg PaddedWaitGroup
		for pb.Next() {
			wg.Go(func() {})
			wg.Wait()
		}
	})
}

func BenchmarkSyncWaitGroupClosure(b *testing.B) {
	type PaddedWaitGroup struct {
		sync.WaitGroup
		pad [128]uint8
	}
	b.RunParallel(func(pb *testing.PB) {
		var wg PaddedWaitGroup
		for pb.Next() {
			wg.Add(1)
			go func() {
				_ = b.N
				wg.Done()
			}()
			wg.Wait()
		}
	})
}

func BenchmarkWaitGroupClosure(b *testing.B) {
	type PaddedWaitGroup struct {
		parallel.WaitGroup
		pad [128]uint8
	}
	b.RunParallel(func(pb *testing.PB) {
		var wg PaddedWaitGroup
		for pb.Next() {
			wg.Go(func() {
				_ = b.N
			})
			wg.Wait()
		}
	})
}
