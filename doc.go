// Package parallel provides a runner to run tasks with limited concurrency.
// Using this package, it should be straightforward to replace any loop with
// similar code that provides concurrency.
package parallel
