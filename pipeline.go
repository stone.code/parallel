package parallel

// CollectErrors creates a pipeline to collect errors from multiple goroutines
// into a slice.  The caller is responsible for calling close on the input
// channel.
//
// The output channel will be blocked until all of the errors have been
// collected.
func CollectErrors(r *Runner) (<-chan []error, chan<- error) {
	// Setup the channels.  Note that we set a buffer for the input to match
	// the max amount of concurrency that the runner will exhibit.
	errin := make(chan error, r.MaxConcurrency())
	errout := make(chan []error, 1)

	// Collect the errors.
	go func() {
		errs := []error(nil)
		for err := range errin {
			errs = append(errs, err)
		}

		errout <- errs
		close(errout)
	}()

	return errout, errin
}
