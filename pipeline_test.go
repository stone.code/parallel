package parallel_test

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"gitlab.com/stone.code/parallel"
)

func ExampleCollectErrors() {
	// Example of a simple loop, which collects errors.
	sum1, err1 := func() (uint32, []error) {
		sum := uint32(0)
		errs := []error(nil)
		for i := 0; i < 10; i++ {
			if i == 5 || i == 7 {
				errs = append(errs, fmt.Errorf("dummy (%d) error", i))
				continue
			}
			atomic.AddUint32(&sum, uint32(i))
		}
		return sum, errs
	}()

	// Modified loop where iterations can run concurrently.
	sum2, err2 := func() (uint32, []error) {
		sum := uint32(0)
		r := parallel.NewRunner(context.Background())
		errout, errin := parallel.CollectErrors(r)
		for i := 0; i < 10; i++ {
			i := i
			r.Go(func() {
				// Note that with enough concurrency, calls with i==5 and calls
				// with i==7 may execute out of order.  To prevent intermittent
				// failures of the test, we deliberately delay i==7.
				if i == 7 {
					time.Sleep(time.Millisecond)
				}
				if i == 5 || i == 7 {
					errin <- fmt.Errorf("dummy (%d) error", i)
					return
				}
				atomic.AddUint32(&sum, uint32(i))
			})
		}
		r.Wait()

		close(errin)
		return sum, <-errout
	}()

	fmt.Println("A:", sum1, err1)
	fmt.Println("B:", sum2, err2)

	// Output:
	// A: 33 [dummy (5) error dummy (7) error]
	// B: 33 [dummy (5) error dummy (7) error]
}
