package parallel

import (
	"sync"
)

// A WaitGroup waits for a collection of callbacks to finish.
// Each callback will be executed in a new goroutine.
//
// A WaitGroup must not be copied after first use.
type WaitGroup struct {
	wg sync.WaitGroup
}

// Go starts a goroutine to execute the callback f, while also maintaining a
// count of the running goroutines.
func (wg *WaitGroup) Go(f func()) {
	wg.wg.Add(1)
	go func() {
		f()
		wg.wg.Done()
	}()
}

// Wait blocks until all of the callbacks have finished executing.
func (wg *WaitGroup) Wait() {
	wg.wg.Wait()
}
