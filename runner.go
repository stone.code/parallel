package parallel

import (
	"context"
	"runtime"
	"sync"
)

// A Runner waits for a collection of callbacks to finish, while running
// the callbacks in a limited number of goroutines.
//
// A Runner must not be copied after first use.
type Runner struct {
	ctx     context.Context
	mu      sync.Mutex
	max     int
	running int
	work    chan func()
	done    chan struct{}
}

// NewRunner returns a new runner based on the provided context.
// The maximum concurrency will be limited to a default value.
//
// See the documentation for NewRunnerWithMax for more details.
func NewRunner(ctx context.Context) *Runner {
	return NewRunnerWithMax(ctx, runtime.NumCPU())
}

// NewRunnerWithMax returns a new runner based on the provided context.
// The maximum concurrency will be limited to max.  As a precondition, the
// maximum concurrency must be greater than zero.  If the maximum concurrency
// is one, then all functions passed to Do will run sequentially.
func NewRunnerWithMax(ctx context.Context, max int) *Runner {
	if ctx == nil {
		ctx = context.Background()
	}
	if max < 1 {
		panic("value of 'max' must be >= 1")
	}
	return &Runner{
		ctx:  ctx,
		max:  max,
		work: make(chan func()),
		done: make(chan struct{}),
	}
}

// Context returns the runners's context.
// The returned context is always non-nil; it defaults to the background context.
func (r *Runner) Context() context.Context {
	return r.ctx
}

// Go adds the function f to the queue of actions to be completed.
// Depending on the runner's maximum concurrency, tasks may be run in parallel,
// and may execute out of order.  Otherwise, tasks will run in the order that
// they are provided.  If there is insufficiency concurrency to start the task,
// the call to Do will block.
//
// If the provided context is either cancelled, either explicitly or because
// it exceeds its deadline, before execution of the function f begins, then
// the function f will not be called.
//
// This method cannot be called after Wait.
func (r *Runner) Go(f func()) {
	select {
	case <-r.ctx.Done():
		return
	case r.work <- f:
		return
	default:
	}

	r.addRunner()

	select {
	case <-r.ctx.Done():
		return
	case r.work <- f:
		return
	}
}

// MaxConcurrency returns the concurrency, which is the maximum number of
// concurrently running routines.
func (r *Runner) MaxConcurrency() int {
	return r.max
}

// Wait marks the runner as complete, and waits for all of the functions
// to terminate.  This method must be called or the runner will leak
// goroutines.
func (r *Runner) Wait() {
	close(r.work)

	for i := 0; i < r.running; i++ {
		<-r.done
	}
}

func (r *Runner) addRunner() {
	r.mu.Lock()
	defer r.mu.Unlock()

	if r.running < r.max {
		r.running++
		go r.runner()
	}
}

func (r *Runner) runner() {
	defer func() {
		r.done <- struct{}{}
	}()

	for {
		select {
		case <-r.ctx.Done():
			// returning not to leak the goroutine
			return

		case action, ok := <-r.work:
			if ok {
				action()
			} else {
				return
			}
		}
	}
}
